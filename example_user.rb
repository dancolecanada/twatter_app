class User
  #creates "getter" and "setter" method
  attr_accessor :name, :email

  #first method called when when we execute User.new, argument attributes
  def initialize(attributes = {})
  	#if no argument is passed in, that hash will return nil for keys without value
    @name  = attributes[:name]
    @email = attributes[:email]
  end

  def formatted_email
    "#{@name} <#{@email}>"
  end
end