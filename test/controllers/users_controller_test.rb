require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
  	@user = users(:daniel)
  	@other_other = users(:julia)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  # Any accidental exposure of the edit methods to unauthorized users will now be caught 
  # immediately by our test suite.
  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end

end
