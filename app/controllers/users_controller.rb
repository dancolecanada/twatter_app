=begin
REpresentational State Transfer (REST)
   that most application components (such as users and microposts) are modeled
   as resources that can be created, read, updated, and deleted—operations that
   correspond both to the CRUD operations of relational databases and to the
   four fundamental HTTP request methods: POST, GET, PATCH, and DELETE.

   POST     create
   GET      read
   PUT      update/replace
   PATCH    update/modify
   DELETE   destroy
=end
class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  #Q: admin can do all functions, why do they still work when i only specify destroy?
  before_action :admin_user,     only:  :destroy

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def new
  	@user = User.new
  end

   def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # Handle a successful update.
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      # Handle an unsuccessful update.
      render 'edit'
    end
  end

  #TODO: read up to see if I can find a way to keep paginate out of controller?
  # Retrieves all users from the db  
  def index
    @users = User.paginate(page: params[:page])
  end

  # Deletes user account, accessible only to admin.
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  # Example of strong params 
  private

  def user_params 				
  	params.require(:user).permit(:name, :email, :password, 
	 	  						 :password_confirmation)
  end

  # Before filters

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end

  # Confirms the correct user.
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end
end
