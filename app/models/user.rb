class User < ApplicationRecord
	# Ensures if I blow up a user all of the users posts will also be destroyed.
  has_many :microposts, dependent: :destroy 

  attr_accessor :remember_token, :activation_token, :reset_token
  before_save   :downcase_email
  before_create :create_activation_digest
	validates :name,  presence: true, 
					  length: { maximum: 50 }
	#TODO: restrict ability to have concurrent '.'
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, 
            length: { maximum: 255 },
					  format: { with: VALID_EMAIL_REGEX },
					  #Q: why omit uniqueness = true when adding case sesitive?
					  # Can that be overidden?
					  uniqueness: { case_sensitive: false }
	# Forces validation on password and password_confirmation				  
	has_secure_password
	# Presence ensure user can't enter 6 spaces
  # Allow nil used to ensure both empty and length validation not presented when empty
  # pw is input. To ensure a user does not input a pw with nil value the method
  # has_secure_password includes a separate presence validation that specifically catches nil passwords
	validates :password,  presence: true, length: { minimum: 6 }, allow_nil: true

  # Returns hash digest of the given string, min cost, min security and max performance
  # Medium or High cost increases security but decreases performance
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
   	                                              BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token
  def User.new_token
  	SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions
  def remember
  	self.remember_token = User.new_token
  	update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Forgets a user
  def forget
  	update_attribute(:remember_digest, nil)
  end

  # Returns true if the token matches the digest
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil? # if remember_digest is nil then execute BCrypt
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Activates an account.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Sends activation email.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Sets the password reset attributes.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Sends password reset email.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Returns true if a password reset has expired.
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  # Defines a proto-feed.
  # See "Following users" for the full implementation.
  def feed
    Micropost.where("user_id = ?", id)
  end

  private

  # Converts email to all lower-case.
  def downcase_email
    self.email = email.downcase
  end

  # Creates and assigns the activation token and digest.
  def create_activation_digest
    self.activation_token  = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
end
