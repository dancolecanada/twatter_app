class ApplicationMailer < ActionMailer::Base
  default from: 'Twatter@donotreplyorelse.com'
  layout 'mailer'
end
