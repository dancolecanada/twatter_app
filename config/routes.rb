Rails.application.routes.draw do

  # Defining routes in this way creates named routes that allow us to refer to 
  # routes by a name rather than by the raw URL.
  root   'static_pages#home'
  get    '/help',     to: 'static_pages#help'
  get    '/about',    to: 'static_pages#about'
  get    '/contact',  to: 'static_pages#contact'
  get    '/signup',   to: 'users#new'
  get    '/login',    to: 'sessions#new'
  post   '/login',    to: 'sessions#create'
  delete '/logout',   to: 'sessions#destroy'
  
  resources :users 
  # All the actions necessary to make Users a fully RESTful resource
  # TODO: read more up on RESTful resourced to ensure everything is covere
  #       for technical.
  # Q: Ask malaz or Lucas if there are any public standards available
  #    which are similar to what Shopify follows.
  #GET:
  #  URL: /users          ACTION: index   NAMED ROUTE: users_path
  #  URL: /users/1        ACTION: show    NAMED ROUTE: users_path(user)
  #  URL: /users/new      ACTION: new     NAMED ROUTE: users_path_new
  #  URL: /users/1/edit   ACTION: edit    NAMED ROUTE: edit_users_path(user)
  #POST:
  #  URL: /users          ACTION: create  NAMED ROUTE: users_path
  #PATCH:
  #  URL: /users/1        ACTION: update  NAMED ROUTE: users_path(users)
  #DELETE:
  #  URL: /users/1        ACTION: destroy NAMED ROUTE: users_path(users)
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
end